#!/bin/sh

case "$1" in
    start)
	echo 'AT^NDISDUP=1,1,"internet"' > /dev/cuaU0.1 && dhclient ue0
	;;
    stop)
	echo 'AT^NDISDUP=1,0' > /dev/cuaU0.1
	echo 'AT^RESET' > /dev/cuaU0.1
	kill -9 `cat /var/run/dhclient.ue0.pid`
	;;
    restart)
	echo 'AT^NDISDUP=1,0' > /dev/cuaU0.1
	kill -9 `cat /var/run/dhclient.ue0.pid`
	echo 'AT^RESET' > /dev/cuaU0.1 &&
	sleep 15
	echo 'AT^NDISDUP=1,1,"internet"' > /dev/cuaU0.1 && dhclient ue0
	;;
    *)
	echo 'AT^NDISDUP=1,1,"internet"' > /dev/cuaU0.1 && dhclient ue0
	;;
esac

#exit 0

