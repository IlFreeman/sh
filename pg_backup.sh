#!/bin/sh

PATH=/etc:/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

DIR=/mnt/backup/
USER=postgres
DB=vbprod1_unf

find $DIR -ctime +20 -delete 
su - $USER -c "/opt/pgpro/std-12/bin/pg_dump -Fc ${DB}" \
       >  $DIR/$DB$(date "+%Y_%m_%d").dump

