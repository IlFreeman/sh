#!/bin/sh

## Building client config for OpenVPN on Ubuntu
## Junior edition v2
HOST=AWS #`hostname`
read -p "What is the name of client?:" name

fname=${HOST}'$'${name}

read -p "Do you want to protect theese key? (y/N):" answer

  case "${answer}" in
          y|Y) read -p "ok, print a password:" pass
             ;;
          n|N) echo " ¯\_(ツ)_/¯ ok, it's easyest way)" && pass=nopass
             ;;
            *) echo "!!!set no password!!!" && pass=nopass
             ;;
  esac

read -p "What is the ${name} operation system? (win/mac/nix):" os

case "$os" in 
    nix) touch . ${fname}\_${os}.ovpn && cat /etc/openvpn/client_nix.conf >> ${fname}\_${os}.ovpn 
       ;;
    mac) touch . ${fname}\_${os}.ovpn && cat /etc/openvpn/client_mac.conf >> ${fname}\_${os}.ovpn 
       ;;
    win) touch . ${fname}\_${os}.ovpn && cat /etc/openvpn/client_win.conf >> ${fname}\_${os}.ovpn 
       ;;
     "")  echo "please do the choise"
       ;;
      *)  echo "please print correctly" && rm -f ${fname}\_${os}.ovpn && exit
       ;;
esac

echo "User: ${name} key  password: ${pass} user OS: ${os}"

cd ~/easy-rsa-ipsec/easyrsa3 && ./easyrsa build-client-full ${name} ${pass}
cat << EOF
  "User : ${name} key password : ${pass} "

  "Collecting the data to file ${fname}\_${os}.ovpn"
EOF

cd ~

cat << EOF >> ${fname}\_${os}.ovpn

###################  certs #####################################
<ca>
`cat ~/easy-rsa-ipsec/easyrsa3/pki/ca.crt`
</ca>
<cert> 
`cat  ~/easy-rsa-ipsec/easyrsa3/pki/issued/$name.crt` 
</cert>
<key>
`cat  ~/easy-rsa-ipsec/easyrsa3/pki/private/$name.key`
</key>
EOF