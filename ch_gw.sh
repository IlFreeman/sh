#!/bin/sh

IP1=192.168.0.61
IP2=`tail -n 15 /var/db/dhclient.leases.ue0 | awk 'FNR==3{{$0=substr($0,1,length($0)-1); print $2}}'`
GW1=192.168.0.1
GW2=`tail -n 15 /var/db/dhclient.leases.ue0 | awk 'FNR==5{{$0=substr($0,1,length($0)-1); print $3}}'`
dtime=`date "+%Y-%m-%d %H:%M:%S"`
TARG1=1.1.1.1
TARG2=8.8.8.8
/sbin/ping -S $IP1 -c 4 $TARG1  > /dev/null 2>&1
if [ $? != 0 ];
then
  /sbin/ping -S $IP2 -c 4 $TARG2  > /dev/null 2>&1
    if [ $? = 0 ];
    then
      if [ ! -f /tmp/gw.changed ];
      then
        /sbin/route change default $GW2 && /bin/echo "${dtime} the gateway was replaced by ${GW2}" >> /var/log/ch-gw.log && touch /tmp/gw.changed
      fi
    fi
    else
      if [ -f /tmp/gw.changed ];
      then
        /sbin/route change default $GW1 && /bin/echo "${dtime} the gateway was replaced by ${GW1}" >> /var/log/ch-gw.log && rm /tmp/gw.changed
      fi
fi
